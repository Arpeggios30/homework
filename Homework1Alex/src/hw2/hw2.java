package hw2;


public class hw2 {

	public static void main(String[] args) {
		SimpleCalculator calculator = new SimpleCalculator();
		calculator.setFirstNumber(5.0);
		calculator.setSecondNumber(5.0);
		System.out.println("add= " + calculator.getAdditionResult());
		System.out.println("substract= " + calculator.getSubtractionResult());
		calculator.setFirstNumber(5.25);
		calculator.setSecondNumber(0);
		System.out.println("multiply= " + calculator.getMultiplicationResult());
		System.out.println("divide= " + calculator.getDivisionResult());
	}

}

class SimpleCalculator {
	double firstNumber;
	double secondNumber;
	double getFirstNumber(){
		return firstNumber;
	}
	double getSecondNumber(){
		return secondNumber;
	}
	void setFirstNumber(double num){
		firstNumber = num;
	}
	void setSecondNumber(double num){
		secondNumber = num;
	}
	double getAdditionResult(){
		return secondNumber + firstNumber;
	}
	double getSubtractionResult() {
		return firstNumber - secondNumber;
	}
	double getMultiplicationResult() {
		return firstNumber * secondNumber;
	}
	double getDivisionResult() {
		return firstNumber / secondNumber;
	}
}
